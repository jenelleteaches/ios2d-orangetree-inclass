//
//  GameScene.swift
//  OrangeTreeInClass
//
//  Created by MacStudent on 2019-02-20.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
   
    // MARK: sprite variables
    var orange:Orange?
    
    // MARK: Variables for aiming the orange
    var mouseStartingPosition:CGPoint = CGPoint(x:0, y:0)
    var lineNode = SKShapeNode()
    
    // MARK: VARAIBLES TO TRACK GAME STATE
    var levelComplete = false
    var currentLevel = 1
    
    
    override func didMove(to view: SKView) {
        
        // add a boundary around the scene
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.name = "wall"
        // initialize delegate
        self.physicsWorld.contactDelegate = self
        
        
        
        // configure the line
        self.lineNode.lineWidth = 20
        self.lineNode.lineCap = .round
        self.lineNode.strokeColor = UIColor.magenta
        addChild(lineNode)
    }
    override func update(_ currentTime: TimeInterval) {
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touch = touches.first  else {
            return
        }

        let mouseLocation = touch.location(in: self)
        print("Finger starting position: \(mouseLocation)")

        // detect what sprite was touched
        let spriteTouched = self.atPoint(mouseLocation)
        
        if (spriteTouched.name == "tree") {
            //print("YOU CLICKED THE TREE")
            // add an orange where the person clicked
            self.orange = Orange()
            orange?.position = mouseLocation
            orange?.name = "orange"
            addChild(self.orange!)
            //self.orange?.physicsBody?.restitution = 1.0
            
            
            self.orange?.physicsBody?.isDynamic = false
            
            // set the starting position of the finger
            self.mouseStartingPosition = mouseLocation
        }

    }
    
    
    // add some more touch functions here
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // 1. get the touch
        guard let touch = touches.first  else {
            return
        }
        
        // 2. get the location
        let mouseLocation = touch.location(in: self)
        
        // 3. update the position of the orange to match the finger
        self.orange?.position = mouseLocation
        
        // 4. draw a line
        let path = UIBezierPath()
        path.move(to:self.mouseStartingPosition)
        path.addLine(to:mouseLocation)
        self.lineNode.path = path.cgPath
        
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touch = touches.first  else {
            return
        }
        
        let mouseLocation = touch.location(in: self)
        print("Finger ending position: \(mouseLocation)")

        // 1. get the ending position of the finger
        let orangeEndingPosition = mouseLocation
        
        // 2. get the difference between finger start & end
        let diffX = orangeEndingPosition.x - self.mouseStartingPosition.x
        let diffY = orangeEndingPosition.y - self.mouseStartingPosition.y
        
        // 3. throw the orange based on that direction
        let direction = CGVector(dx: diffX, dy: diffY)
        self.orange?.physicsBody?.isDynamic = true
        self.orange?.physicsBody?.applyImpulse(direction)

        
        // 5. remove the line form the drawing
        self.lineNode.path = nil
        
    }
    
    
    // MARK:  SKPhysicsContactDelegate functions
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if (levelComplete == true) {
            return
        }
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        if (contact.collisionImpulse > 40) {
            if (nodeA?.name == "skull") {
                print("Skull hit: \(nodeB?.name)")
                print("Impact amount: \(contact.collisionImpulse)")
                print("--------")
                
                
                // animate and remove the skull
                let reduceImageSize = SKAction.scale(by: 0.8, duration: 0.5)
                let removeNode = SKAction.removeFromParent()
                
                let sequence = SKAction.sequence([reduceImageSize, removeNode])
                
                nodeA?.run(sequence)
                
                self.gameOver()
            }
            else if (nodeB?.name == "skull") {
                print("Skull hit: \(nodeA?.name)")
                print("Impact amount: \(contact.collisionImpulse)")
                print("--------")
                
                // animate and remove the skull
                let reduceImageSize = SKAction.scale(by: 0.8, duration: 0.5)
                let removeNode = SKAction.removeFromParent()
                
                let sequence = SKAction.sequence([reduceImageSize, removeNode])
                
                nodeB?.run(sequence)
                
                self.gameOver()
            }
        }
    }
    
   
    @objc func restartGame() {
        // load Level2.sks
        let scene = GameScene(fileNamed:"Level2")
        scene!.scaleMode = scaleMode
        view!.presentScene(scene)
    }
    
    func setLevel(levelNumber:Int) {
        self.currentLevel = levelNumber
    }
    
func gameOver() {
    self.levelComplete = true
    
    // increase the level number
    let message = SKLabelNode(text:"LEVEL \(self.currentLevel) COMPLETE!")
    message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
    message.fontColor = UIColor.magenta
    message.fontSize = 100
    message.fontName = "AvenirNext-Bold"
    
    addChild(message)
    
    // load the next level
    self.currentLevel = self.currentLevel + 1
    
    // try to load the next level
    guard let nextLevelScene = GameScene.loadLevel(levelNumber: self.currentLevel) else {
        print("Error when loading next level")
        return
    }
    
    // wait 1 second then switch to next leevl
    let waitAction = SKAction.wait(forDuration:1)
    
    let showNextLevelAction = SKAction.run {
        nextLevelScene.setLevel(levelNumber: self.currentLevel)
        let transition = SKTransition.flipVertical(withDuration: 2)
        nextLevelScene.scaleMode = .aspectFill
        self.scene?.view?.presentScene(nextLevelScene, transition:transition)
    }
    
    let sequence = SKAction.sequence([waitAction, showNextLevelAction])
    
    self.run(sequence)
    
//        perform(#selector(GameScene.restartGame), with: nil,
//                afterDelay: 3)
}
    
   
    
    class func loadLevel(levelNumber:Int) -> GameScene? {
        let fileName = "Level\(levelNumber)"
        
        // DEBUG nonsense
        print("Trying to load file: Level\(levelNumber).sks")
        
        guard let scene = GameScene(fileNamed: fileName) else {
            print("Cannot find level named: \(levelNumber).sks")
            return nil
        }
        
        scene.scaleMode = .aspectFill
        return scene
        
    }
    
    
    
    
    
    
}
